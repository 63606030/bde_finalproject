from airflow.decorators import dag, task
from datetime import datetime
# from airflow.providers.mysql.operators.mysql import MySqlOperator
from airflow.providers.mysql.hooks.mysql import MySqlHook
# import json
import requests
import pandas as pd
from bs4 import BeautifulSoup

default_args = {
    'start_date': datetime(2021, 12, 1)
}

@dag('python_ingest_lotto_dag', schedule_interval='@daily', default_args=default_args, catchup=False)
# def taskflow():
def myTask():

    @task
    def step00_delete_lotto():
        year = "2564"
        y = int(year)-543
        query = "DELETE FROM `lotto` WHERE year(lottery_date) = '"+str(y)+"'"
        hook = MySqlHook(mysql_conn_id='mysql_testdb')
        hook.run(sql=query)
        print("Delete table lotto Success")
        
        return "Delete table lotto Success"

    # Function Convert date
    def switch_month(argument):
        switcher = {
                "มกราคม": "01",
                "กุมภาพันธ์": "02",
                "มีนาคม": "03",
                "เมษายน": "04",
                "พฤษภาคม": "05",
                "มิถุนายน": "06",
                "กรกฎาคม": "07",
                "สิงหาคม": "08",
                "กันยายน": "09",
                "ตุลาคม": "10",
                "พฤศจิกายน": "11",
                "ธันวาคม": "12"
        }
        return switcher.get(argument, "Invalid month")


    @task
    def step01_ingest_lotto():
        year = "2564"
        url = 'https://www.myhora.com/%E0%B8%AB%E0%B8%A7%E0%B8%A2/%e0%b8%9b%e0%b8%b5-'+year+'.aspx'
        data = requests.get(url)
        soup = BeautifulSoup(data.text,'html.parser')
        date = soup.find_all("font",{"style":"font-size:1.2em;"})
        ltr_dc = soup.find_all("div",{"class":"lot-dc lotto-fxl"})

        data_list = []
        no1 = ''
        no3_1 = ''
        no3_2 = ''
        no4 = ''
        round = 0

        for i_number in ltr_dc:

            if len(str(i_number.text)) == 6:
                no1 = i_number.text
            elif len(str(i_number.text)) == 9 and no3_1 == '' and no3_2 == '':
                no3_1 = i_number.text
            elif len(str(i_number.text)) == 9 and no3_1 != '' and no3_2 == '':
                no3_2 = i_number.text
            elif len(str(i_number.text)) == 2:
                no4 = i_number.text
            
            if no4 != '':
                date_round = date[round].text.replace("ตรวจสลากกินแบ่งรัฐบาล งวด",'')
                arr_date = date_round[1:].split(" ")

                if len(str(arr_date[0])) == 1:
                    d = '0'+str(arr_date[0])
                else:
                    d = arr_date[0]
                m = switch_month(arr_date[2])
                y = int(arr_date[3])-543
                Date_new_format = str(y)+'-'+str(m)+'-'+str(d)
                data_list.append([Date_new_format, no1, no3_1[0:3], no3_1[-3:], no3_2[0:3], no3_2[-3:], no4])

                no1 = ''
                no3_1 = ''
                no3_2 = ''
                no4 = ''
                round = round + 1

        columns = ['LotteryDate','FirstPrize','FirstThreeDg_1','FirstThreeDg_2','LastThreeDg_1','LastThreeDg_2','LastTwoDg']
        df = pd.DataFrame(data_list, columns=columns)
        return df.values.tolist()
        # return df.to_json()

    @task
    def step02_load_to_mysql(val):
        values = ""
        for row in val:
            member_of_values = ""
            for column in row:
                member_of_values += "'{}',".format(column)
            values += "({}),".format(member_of_values[:-1])
        
        query = """insert into lotto (lottery_date,first_prize,first_three_dg_1,first_three_dg_2,last_three_dg_1,last_three_dg_2,last_two_dg) 
                values {}""".format(values[:-1])

        hook = MySqlHook(mysql_conn_id='mysql_testdb')
        hook.run(sql=query)

    step02_load_to_mysql(step00_delete_lotto() >> step01_ingest_lotto())

dag = myTask()