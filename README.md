# data-engineer-lab

#Start Docker-Compose
    docker-compose up -d

#Add Connection
    * Connection Id : mysql_testdb
    * Connection Type : MySQL
    * Host : mysql
    * Schema : testdb
    * Login : testdb
    * Password : testdb
    * Port : 3306

#Stop Docker-Compose
    docker-compose down 

#Ari Flow
http://localhost:8080/
u:admin
p:password

#phpmyadmin
http://localhost:8088/
u:testdb
p:testdb